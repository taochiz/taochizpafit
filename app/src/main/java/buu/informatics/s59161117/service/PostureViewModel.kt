package buu.informatics.s59161117.service

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import buu.informatics.s59161117.database.AppDatabase
import buu.informatics.s59161117.database.PostureModel
import kotlinx.coroutines.launch

class PostureViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: PostureRepository
    val allPosture: LiveData<List<PostureModel>>

    init {
        val postureDAO = AppDatabase.getDatabase(application.applicationContext).postureDatabaseDAO()
        repository = PostureRepository(postureDAO)
        allPosture = repository.allPosture
    }

    fun insert(posture: PostureModel) = viewModelScope.launch {
        repository.insert(posture)
    }

    fun delete(item:String) = viewModelScope.launch {
        repository.delete(item)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }
}