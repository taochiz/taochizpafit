package buu.informatics.s59161117.service

import androidx.lifecycle.LiveData
import buu.informatics.s59161117.database.FavoriteDAO
import buu.informatics.s59161117.database.FavoriteModel
import buu.informatics.s59161117.database.PostureDAO
import buu.informatics.s59161117.database.PostureModel

class FavoriteRepository (private val favoriteDAO: FavoriteDAO) {

    val allFavorite: LiveData<List<FavoriteModel>> = favoriteDAO.getAll()

    fun insert(favorite: FavoriteModel) {
        favoriteDAO.insert(favorite)
    }

    fun delete(item: String){
        favoriteDAO.delete(item)
    }

    fun deleteAll(){
        favoriteDAO.deleteAll()
    }
}