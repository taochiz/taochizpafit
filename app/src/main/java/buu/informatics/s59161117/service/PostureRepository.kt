package buu.informatics.s59161117.service

import androidx.lifecycle.LiveData
import buu.informatics.s59161117.database.PostureDAO
import buu.informatics.s59161117.database.PostureModel

class PostureRepository (private val postureDAO: PostureDAO) {

    val allPosture: LiveData<List<PostureModel>> = postureDAO.getAll()

    fun insert(posture: PostureModel) {
        postureDAO.insert(posture)
    }

    fun delete(item: String){
        postureDAO.delete(item)
    }

    fun deleteAll(){
        postureDAO.deleteAll()
    }
}