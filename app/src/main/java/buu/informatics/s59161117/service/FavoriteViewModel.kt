package buu.informatics.s59161117.service

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import buu.informatics.s59161117.database.AppDatabase
import buu.informatics.s59161117.database.FavoriteModel
import buu.informatics.s59161117.database.PostureModel
import kotlinx.coroutines.launch

class FavoriteViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: FavoriteRepository
    val allFavorite: LiveData<List<FavoriteModel>>

    init {
        val favoriteDAO = AppDatabase.getDatabase(application.applicationContext).favoriteDatabaseDAO()
        repository = FavoriteRepository(favoriteDAO)
        allFavorite = repository.allFavorite
    }

    fun insert(favorite: FavoriteModel) = viewModelScope.launch {
        repository.insert(favorite)
    }

    fun delete(item:String) = viewModelScope.launch {
        repository.delete(item)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }
}