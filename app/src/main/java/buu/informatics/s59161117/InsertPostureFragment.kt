package buu.informatics.s59161117


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.s59161117.database.PostureModel
import buu.informatics.s59161117.databinding.FragmentInsertPostureBinding
import buu.informatics.s59161117.service.PostureViewModel
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_insert_posture.*
import java.util.*
import kotlin.collections.HashMap

/**
 * A simple [Fragment] subclass.
 */
class InsertPostureFragment : Fragment() {

    private lateinit var binding: FragmentInsertPostureBinding
    private lateinit var postureViewModel: PostureViewModel
    private var filePath: Uri? = null
    private var firebaseStore: FirebaseStorage? = null
    private var storageReference: StorageReference? = null
    private var firebaseAuth = FirebaseAuth.getInstance()
    private var urlVideo = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_insert_posture, container, false)

//        val arrayExercise = arrayOf("หน้าท้อง","หลัง","กล้ามเนื้อต้นแขนส่วนหน้า","น่อง","หน้าอก","แขนท่อนล่าง","ขา","ไหล่", "กล้ามเนื้อท่อนแขนส่วนบนด้านหลัง")
//        val spinner = binding.exerciseSpinner
//        spinner.adapter = ArrayAdapter(activity!!, R.layout.support_simple_spinner_dropdown_item, arrayExercise)

        firebaseStore = FirebaseStorage.getInstance()
        storageReference = FirebaseStorage.getInstance().getReference()
        postureViewModel = ViewModelProviders.of(this).get(PostureViewModel::class.java)

        connectFirebase("59161117@go.buu.ac.th", "isylot555+")
        onCheckPermission()
        onInsertImage()
        onSave()


        binding.clearButton.setOnClickListener {
            onClear()
        }

        return binding.root
    }


    private fun onSave() {
        binding.saveButton.setOnClickListener {
            uploadImage()
            Log.i("URL", "${urlVideo}")
            var postureText = postureEditText.text.toString()
            var detailText = detailEditText.text.toString()
            Handler().postDelayed({
                if (!postureText.isEmpty() && !detailText.isEmpty()) {
                    if (urlVideo != "") {
                        Handler().postDelayed({
                            val posture =
                                PostureModel(
                                    postureText,
                                    detailText,
                                    urlVideo
                                )
                            postureViewModel.insert(posture)
                            Toast.makeText(getActivity(), "เพื่อข้อมูลสำเร็จ!!", Toast.LENGTH_LONG)
                                .show()
                        }, 2000)
                        onClear()
                        view!!.findNavController().navigate(InsertPostureFragmentDirections.actionInsertPostureFragmentToExerciseFragment())
                    } else {
                        Toast.makeText(getActivity(), "เลือกวีดีโอ!!", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(getActivity(), "กรอกข้อมูลให้ครบ!!", Toast.LENGTH_LONG)
                        .show()
                }
            }, 5000)
        }

    }

    private fun onInsertImage() {
        binding.videoButton.setOnClickListener {

            //Intent to pick image
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "video/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)

            Log.i("filePath", filePath.toString())
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        filePath = data?.data
        videoButton.text = getText(R.string.videoSelect)
    }

    private fun addUploadRecordToDb(uri: String) {
        val db = FirebaseFirestore.getInstance()
        val data = HashMap<String, Any>()
        data["videoUrl"] = uri
        db.collection("posts")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.i("DB", "Saved to DB")
            }
            .addOnFailureListener { e ->
                Log.i("DB", "Error saving to DB")
            }
    }

    private fun uploadImage() {
        if (filePath != null) {
            val ref = storageReference?.child("uploads/" + UUID.randomUUID().toString())
            val uploadTask = ref?.putFile(filePath!!)
            Log.i("video", uploadTask.toString())
            val urlTask =
                uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    return@Continuation ref.downloadUrl
                })?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result
                        Log.i("video", downloadUri.toString())
                        urlVideo = downloadUri.toString()
                        addUploadRecordToDb(downloadUri.toString())
                    } else {
                        // Handle failures
                    }
                }?.addOnFailureListener {

                }
        }
    }


    private fun onClear() {
        binding.postureEditText.setText("")
        binding.detailEditText.setText("")
    }

    private fun connectFirebase(email: String, password: String) {
        Log.e("Firebase", "Auth........")
        firebaseAuth.signInWithEmailAndPassword(email, password)
            ?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.e("Firebase", "successful")
//                    var intent = Intent(getActivity(), InsertPostureFragment::class.java)
//                    intent.putExtra("id", firebaseAuth.currentUser?.email)
//                    startActivity(intent)
                    val user = this.firebaseAuth.currentUser!!

                } else {
                    Log.e("Firebase", "${task.exception?.message}")
                }
            }
    }

    private fun onCheckPermission() {
        if (getActivity()?.applicationContext?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            } != PackageManager.PERMISSION_GRANTED) {
            val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
            requestPermissions(permissions, PERMISSION_CODE)

        }
    }

    companion object {
        private val IMAGE_PICK_CODE = 1000
        private val PERMISSION_CODE = 1001
    }

}
