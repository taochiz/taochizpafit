package buu.informatics.s59161117


import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.android.marsrealestate.network.Article
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso

class ListNewsAdapter : RecyclerView.Adapter<ListNewsAdapter.ViewHolder>() {

    var picasso = Picasso.get()
    var data = listOf<Article>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleText: TextView = itemView.findViewById(R.id.titleText)
        val imageView: ImageView = itemView.findViewById(R.id.imageView)
        val viewButton: Button = itemView.findViewById(R.id.viewButton)

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.view_items_news, parent, false)
                return ViewHolder(view)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = data[position]
        holder.bind(item)
    }

    private fun ViewHolder.bind(item: Article) {
        titleText.text = item.title
        imageView.setImageURI(Uri.parse(item.urlToImage))
        picasso.load(item.urlToImage).into(imageView)

        viewButton.setOnClickListener {
            Snackbar.make(viewButton, "${item.url}", Snackbar.LENGTH_LONG).setAction("คัดลอก") {

            }.show()

        }

    }
}
