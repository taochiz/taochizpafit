package com.example.android.marsrealestate.network

data class Source(
    val id: Any?,
    val name: String?
)