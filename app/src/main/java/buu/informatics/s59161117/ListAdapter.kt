package buu.informatics.s59161117

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import buu.informatics.s59161117.service.FavoriteViewModel

class ListAdapter: RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    var data = listOf<ListView>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView){
        val nameText: TextView = itemView.findViewById(R.id.listviewText)
        val imageView: ImageView = itemView.findViewById(R.id.listviewImage)
        val deleteButton: Button = itemView.findViewById(R.id.deleteButton)
        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.fragment_listview, parent, false)
                return ViewHolder(view)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = data[position]
        holder.bind(item)
    }

    private fun ViewHolder.bind(item: ListView) {
        nameText.text = item.name
        imageView.setImageResource(item.image)
        if(item.id.contains("f")){
            deleteButton.visibility = View.VISIBLE
            deleteButton.setOnClickListener {
                nameText.visibility = View.GONE
                imageView.visibility = View.GONE
                deleteButton.visibility = View.GONE
                Log.i("delete",item.name)
            }
        }
    }
}
