package buu.informatics.s59161117


import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.AbsListView
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.RecyclerView
import buu.informatics.s59161117.databinding.FragmentFavoriteBinding
import buu.informatics.s59161117.service.FavoriteViewModel
import com.hudomju.swipe.SwipeToDismissTouchListener
import com.hudomju.swipe.adapter.ListViewAdapter
import kotlinx.android.synthetic.main.fragment_favorite.*
import kotlinx.android.synthetic.main.fragment_listview.view.*

/**
 * A simple [Fragment] subclass.
 */
class FavoriteFragment : Fragment() {

    private lateinit var binding: FragmentFavoriteBinding
    private lateinit var favoriteViewModel: FavoriteViewModel
    private lateinit var listViewFragment: ListViewFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false)

        favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel::class.java)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val arrayFavorite: ArrayList<ListView> = insertFavorite()
//        listFavorites.adapter = ListViewFragment(getActivity()?.applicationContext, arrayFavorite)
//        listFavorites.setOnItemClickListener { parent, view, position, id ->
//            view.findNavController()
//                .navigate(
//                    FavoriteFragmentDirections.actionFavoriteFragmentToPostureDetailFragment(
//                        arrayFavorite[position].id + " " + arrayFavorite[position].name
//                    )
//                )
//        }

        val adapter = ListAdapter()
        binding.recyclerViewFavorites.adapter = adapter

        adapter.data = arrayFavorite

        binding.recyclerViewFavorites.addOnItemClickListener(object :
            OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                view.findNavController()
                    .navigate(
                        FavoriteFragmentDirections.actionFavoriteFragmentToPostureDetailFragment(
                            arrayFavorite[position].id + " " + arrayFavorite[position].name
                        )
                    )
            }
        })


        setHasOptionsMenu(true)
    }

//    private fun setSwipeDelete(arrayFavorite: ArrayList<ListView>) {
//
//        val touchListener = SwipeToDismissTouchListener(
//            ListViewAdapter(listFavorites),
//            object : SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter> {
//                override fun canDismiss(position: Int): Boolean {
//                    return true
//                }
//
//                override fun onDismiss(view: ListViewAdapter, position: Int) {
//                    val posture =
//                        listFavorites.get(position).findViewById(R.id.nameText) as TextView
////                    Log.i("position", ttd.getText().toString());
//                    listViewFragment?.remove(position)
//                    favoriteViewModel.delete(posture.getText().toString())
//                }
//            })
//        listFavorites!!.setOnTouchListener(touchListener)
//        listFavorites!!.setOnScrollListener(touchListener.makeScrollListener() as AbsListView.OnScrollListener)
//        listFavorites!!.onItemClickListener =
//            AdapterView.OnItemClickListener { parent, view, position, id ->
//                if (touchListener.existPendingDismisses()) {
//                    touchListener.undoPendingDismiss()
//                } else {
//                    Handler().postDelayed({
//                            view.findNavController()
//                                .navigate(
//                                    FavoriteFragmentDirections.actionFavoriteFragmentToPostureDetailFragment(arrayFavorite[position].id + " " + arrayFavorite[position].name)
//                                )
//                    }, 200)
//                }
//            }
//    }

    private fun insertFavorite(): ArrayList<ListView> {
        var array: ArrayList<ListView> = ArrayList()
        var count = 0
        Handler().postDelayed({
            favoriteViewModel.allFavorite.observe(this, Observer { favorite ->
                favorite.forEach {
                    Log.i("ViewModel", it.postureName)
                    array.add(ListView("f${count}", it.postureName, 0))
                    count++
                }
                if (count == favorite.size) {
                    val adapter = ListAdapter()
                    binding.recyclerViewFavorites.adapter = adapter

                    adapter.data = array
//                    setSwipeDelete(array)
                }
            })
        }, 1000)
        return array
    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
                view?.setOnClickListener(null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view?.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (menu != null) {
            if (inflater != null) {
                super.onCreateOptionsMenu(menu, inflater)
            }
        }
        inflater?.inflate(R.menu.options_menu, menu)
        menu.getItem(0).setVisible(false)
        menu.getItem(1).setVisible(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item!!,
            view!!.findNavController()
        )
                || super.onOptionsItemSelected(item)
    }

}
