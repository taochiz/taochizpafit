package buu.informatics.s59161117


import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import buu.informatics.s59161117.databinding.FragmentPostureBinding
import buu.informatics.s59161117.service.PostureViewModel

/**
 * A simple [Fragment] subclass.
 */
class PostureFragment : Fragment() {

    private lateinit var binding: FragmentPostureBinding
    private lateinit var postureViewModel: PostureViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_posture,
            container,
            false
        )

        postureViewModel = ViewModelProviders.of(this).get(PostureViewModel::class.java)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val arrayPosture: ArrayList<ListView> = insertPosture()
//        listPostures.adapter = ListViewFragment(getActivity()?.applicationContext, arrayPosture)
//        listPostures.setOnItemClickListener { parent, view, position, id ->
//            view.findNavController()
//                .navigate(
//                    PostureFragmentDirections.actionPostureFragmentToPostureDetailFragment(
//                        arrayPosture[position].id + " " + arrayPosture[position].name
//                    )
//                )
//        }

        val adapter = ListAdapter()
        binding.recyclerViewPostures.adapter = adapter

        adapter.data = arrayPosture

        binding.recyclerViewPostures.addOnItemClickListener(object :
            OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                view.findNavController()
                    .navigate(
                        PostureFragmentDirections.actionPostureFragmentToPostureDetailFragment(
                            arrayPosture[position].id + " " + arrayPosture[position].name
                        )
                    )
            }
        })

        setHasOptionsMenu(true)
    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
                view?.setOnClickListener(null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view?.setOnClickListener({
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                })
            }
        })
    }

    private fun insertPosture(): ArrayList<ListView> {
        val getExercise = arguments?.getString("exercise")!!.split(" ")
        val arrayPosture: ArrayList<ListView> = ArrayList()
        (activity as AppCompatActivity).supportActionBar?.title = getExercise.get(1)

        if (getExercise.get(0) == "ex1") {
            arrayPosture.add(
                ListView(
                    "pt11",
                    "ท่าฝึกกล้ามเนื้อหน้าท้องแบบงอตัว",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex2") {
            arrayPosture.add(
                ListView(
                    "pt21",
                    "ท่ายกตัวซุปเปอร์แมน",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex3") {
            arrayPosture.add(
                ListView(
                    "pt31",
                    "ท่ายกดัมเบลแขนโค้งงอ",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex4") {
            arrayPosture.add(
                ListView(
                    "pt41",
                    "ยกนิ้วเท้า",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex5") {
            arrayPosture.add(
                ListView(
                    "pt51",
                    "วิดพื้น",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex6") {
            arrayPosture.add(
                ListView(
                    "pt61",
                    "ท่าหมุนบิดดัมเบลด้วยข้อมูล",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex7") {
            arrayPosture.add(
                ListView(
                    "pt71",
                    "ท่าย่อตัวด้วยขาข้างเดียว",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex8") {
            arrayPosture.add(
                ListView(
                    "pt81",
                    "ท่ายกดัมเบลด้วยไหล่",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex9") {
            arrayPosture.add(
                ListView(
                    "pt91",
                    "ท่านอนตะแคงวิดพื้นด้วยแขนหนึ่งข้าง",
                    0
                )
            )
        } else if (getExercise.get(0) == "ex10") {
            var count = 0
            Handler().postDelayed({
                postureViewModel.allPosture.observe(this, Observer { posture ->
                    posture.forEach {
                        Log.i("ViewModel", it.postureName)
                        arrayPosture.add(ListView("pt0${count}", it.postureName, 0))
                        count++
                    }
                    if (count == posture.size) {
                        val adapter = ListAdapter()
                        binding.recyclerViewPostures.adapter = adapter

                        adapter.data = arrayPosture
                    }
                })
            }, 200)

        }

        return arrayPosture
    }
}
