package buu.informatics.s59161117.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(PostureModel::class, FavoriteModel::class), version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun postureDatabaseDAO() : PostureDAO

    abstract fun favoriteDatabaseDAO() : FavoriteDAO

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(
            context: Context
        ): AppDatabase {
            return INSTANCE?: synchronized(this) {
                var instance = Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "taochizpafit"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                //return instance
                instance
            }
        }
    }
}
