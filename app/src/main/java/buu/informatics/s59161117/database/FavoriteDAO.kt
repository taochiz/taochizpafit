package buu.informatics.s59161117.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FavoriteDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(favorite: FavoriteModel)

    @Query("SELECT * FROM favorite_items ORDER BY postureName ASC")
    fun getAll(): LiveData<List<FavoriteModel>>

    @Query("DELETE FROM favorite_items ")
    fun deleteAll()

    @Query("DELETE FROM favorite_items WHERE postureName = :favoriteSelected ")
    fun delete(favoriteSelected: String)

}