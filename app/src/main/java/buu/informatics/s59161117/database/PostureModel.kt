package buu.informatics.s59161117.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posture_items")
data class PostureModel(

    @PrimaryKey

    @ColumnInfo(name = "postureName")
    val postureName: String,

    @ColumnInfo(name = "postureDetail")
    val postureDetail: String,

    @ColumnInfo(name = "postureVideo")
    val postureVideo: String
)