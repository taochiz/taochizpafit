package buu.informatics.s59161117.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_items")
data class FavoriteModel(

    @PrimaryKey

    @ColumnInfo(name = "postureName")
    val postureName: String
)