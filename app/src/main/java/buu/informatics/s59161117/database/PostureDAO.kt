package buu.informatics.s59161117.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PostureDAO {

    @Query("SELECT * FROM posture_items ORDER BY postureName ASC")
    fun getAll(): LiveData<List<PostureModel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(exercise: PostureModel)

    @Query("DELETE FROM posture_items ")
    fun deleteAll()

    @Query("DELETE FROM posture_items WHERE postureName = :postureSelected ")
    fun delete(postureSelected: String)
}