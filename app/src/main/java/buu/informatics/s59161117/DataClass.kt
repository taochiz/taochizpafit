package buu.informatics.s59161117

data class ListView(var id: String, var name: String, var image: Int)

data class News(var title: String?, var image: String?, var url: String?)