package buu.informatics.s59161117

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import buu.informatics.s59161117.databinding.FragmentNewsBinding

import buu.informatics.s59161117.service.NewsViewModel
import com.example.android.marsrealestate.network.Article

class NewsFragment : Fragment() {

    private val viewModel: NewsViewModel by lazy {
        ViewModelProviders.of(this).get(NewsViewModel::class.java)
    }

    private lateinit var binding: FragmentNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_news,
            container,
            false
        )
        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment

        insertNews()
        return binding.root
    }

    private fun insertNews() {
        var array: ArrayList<Article> = ArrayList()
        var count = 0
        Handler().postDelayed({
            viewModel.properties.observe(this, Observer { list ->
                Log.i("test", "${list.size}")
                list.forEach {
                    if(it.urlToImage != null){
                        array.add(
                            Article(
                                it.author,
                                it.content,
                                it.description,
                                it.publishedAt,
                                it.source,
                                it.title,
                                it.url,
                                it.urlToImage
                            )
                        )
                    }
                    count++
                }
                if (count == list.size) {
                    Log.i("test","count ${count}")
                    val adapter = ListNewsAdapter()
                    binding.recyclerViewNews.adapter = adapter
                    adapter.data = array

                }
            })

        }, 1000)
    }
}

