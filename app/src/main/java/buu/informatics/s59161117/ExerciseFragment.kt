package buu.informatics.s59161117


import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.ListAdapter
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import buu.informatics.s59161117.databinding.FragmentExerciseBinding
import kotlinx.android.synthetic.main.fragment_exercise.*

/**
 * A simple [Fragment] subclass.
 */
class ExerciseFragment : Fragment() {

    private lateinit var binding: FragmentExerciseBinding
    //    private val array = listOf(
//        ListView("ex1", "หน้าท้อง", R.drawable.ex1),
//        ListView("ex2", "หลัง", R.drawable.ex2),
//        ListView("ex3", "กล้ามเนื้อต้นแขนส่วนหน้า", R.drawable.ex3),
//        ListView("ex4", "น่อง", R.drawable.ex4),
//        ListView("ex5", "หน้าอก", R.drawable.ex5),
//        ListView("ex6", "แขนท่อนล่าง", R.drawable.ex6),
//        ListView("ex7", "ขา", R.drawable.ex7),
//        ListView("ex8", "ไหล่", R.drawable.ex8),
//        ListView("ex9", "กล้ามเนื้อท่อนแขนส่วนบนด้านหลัง", R.drawable.ex9),
//        ListView("ex10", "อื่นๆ", R.drawable.ex10)
//    )
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exercise, container, false)

        var array: ArrayList<ListView> = ArrayList()
        array.add(ListView("ex1", "หน้าท้อง", R.drawable.ex1))
        array.add(ListView("ex2", "หลัง", R.drawable.ex2))
        array.add(ListView("ex3", "กล้ามเนื้อต้นแขนส่วนหน้า", R.drawable.ex3))
        array.add(ListView("ex4", "น่อง", R.drawable.ex4))
        array.add(ListView("ex5", "หน้าอก", R.drawable.ex5))
        array.add(ListView("ex6", "แขนท่อนล่าง", R.drawable.ex6))
        array.add(ListView("ex7", "ขา", R.drawable.ex7))
        array.add(ListView("ex8", "ไหล่", R.drawable.ex8))
        array.add(ListView("ex9", "กล้ามเนื้อท่อนแขนส่วนบนด้านหลัง", R.drawable.ex9))
        array.add(ListView("ex10", "อื่นๆ", R.drawable.ex10))

        val adapter = ListAdapter()
        binding.recyclerViewExercises.adapter = adapter

        adapter.data = array

        binding.recyclerViewExercises.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                view.findNavController()
                    .navigate(
                        ExerciseFragmentDirections.actionExerciseFragmentToPostureFragment(
                            array[position].id + " " + array[position].name
                        )
                    )
            }
        })


//        binding.listViewExercises.adapter = ListViewFragment(getActivity()?.applicationContext, array)
//
//
//        binding.listViewExercises.setOnItemClickListener { parent, view, position, id ->
//            view.findNavController()
//                .navigate(ExerciseFragmentDirections.actionExerciseFragmentToPostureFragment(array[position].id + " " + array[position].name))
//        }
        setHasOptionsMenu(true)
        return binding.root

    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
                view?.setOnClickListener(null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view?.setOnClickListener({
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                })
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (menu != null) {
            if (inflater != null) {
                super.onCreateOptionsMenu(menu, inflater)
            }
        }
        inflater?.inflate(R.menu.options_menu, menu)
        menu.getItem(0).setVisible(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item!!,
            view!!.findNavController()
        )
                || super.onOptionsItemSelected(item)
    }

}
