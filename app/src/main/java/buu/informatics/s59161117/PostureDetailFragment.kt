package buu.informatics.s59161117


import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.MediaController
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59161117.database.FavoriteModel
import buu.informatics.s59161117.databinding.FragmentPostureDetailBinding
import buu.informatics.s59161117.service.FavoriteViewModel
import buu.informatics.s59161117.service.PostureViewModel

/**
 * A simple [Fragment] subclass.
 */
class PostureDetailFragment : Fragment() {

    lateinit var binding: FragmentPostureDetailBinding
    private var videoView: VideoView? = null
    private var mediacontroller: MediaController? = null
    private lateinit var postureViewModel: PostureViewModel
    private lateinit var favoriteViewModel: FavoriteViewModel
    private var menuFavorite: Menu? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_posture_detail,
            container,
            false
        )

        postureViewModel = ViewModelProviders.of(this).get(PostureViewModel::class.java)
        favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel::class.java)


        videoView = binding.videoView as VideoView

        mediacontroller = MediaController(activity)
        mediacontroller!!.setAnchorView(videoView)

        setPosture()

        setPostureOfDatabase()

        videoView!!.setMediaController(mediacontroller)

        videoView!!.start()
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun insertFavorite() {
        val getPosture = arguments?.getString("posture")!!.split(" ")
        val favorite = FavoriteModel(getPosture.get(1)!!)
        favoriteViewModel.insert(favorite)
    }


    private fun setFavorite(posture: String?) {
        favoriteViewModel.allFavorite.observe(this, Observer { favorite ->
            favorite.forEach {
                if (it.postureName == posture) {
                    menuFavorite?.getItem(0)?.icon = activity?.applicationContext?.let { it ->
                        ContextCompat.getDrawable(
                            it, android.R.drawable.btn_star_big_on
                        )
                    }
                }
            }
        })
    }

    private fun setPostureOfDatabase() {
        val getPosture = arguments?.getString("posture")!!.split(" ")
        (activity as AppCompatActivity).supportActionBar?.title = getPosture.get(1)

        Handler().postDelayed({
            postureViewModel.allPosture.observe(this, Observer { posture ->
                posture.forEach {
                    Log.i("ViewModel",it.postureName)
                    if(getPosture.get(1) == it.postureName){
                        videoView!!.setVideoURI(Uri.parse("${it.postureVideo}"))
                        binding.textView.text = it.postureDetail
                    }
                }
            })
        }, 200)
        setFavorite(getPosture.get(1))
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (menu != null) {
            if (inflater != null) {
                super.onCreateOptionsMenu(menu, inflater)
            }
        }
        inflater?.inflate(R.menu.options_menu, menu)
        menu.getItem(1).setVisible(false)
        this.menuFavorite = menu
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favoriteIcon -> insertFavorite()
        }
        return super.onOptionsItemSelected(item)

    }


    private fun setPosture() {
        val getPosture = arguments?.getString("posture")!!.split(" ")
        (activity as AppCompatActivity).supportActionBar?.title = getPosture.get(1)

        if (getPosture.get(1) == "ท่าฝึกกล้ามเนื้อหน้าท้องแบบงอตัว") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit.appspot.com/o/uploads%2Fpt11.mp4?alt=media&token=7f449f87-aa80-48d5-86e9-0e051bcb5397"))
            binding.textView.text = getString(R.string.pt11)

        } else if (getPosture.get(1) == "ท่ายกตัวซุปเปอร์แมน") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt21.mp4?alt=media&token=71900779-7fcb-4f20-93b8-4395e9208110"))
            binding.textView.text = getString(R.string.pt21)

        } else if (getPosture.get(1) == "ท่ายกดัมเบลแขนโค้งงอ") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt31.mp4?alt=media&token=d99907ec-6bb3-4a08-91fd-4c2e3cea9f4b"))
            binding.textView.text = getString(R.string.pt31)

        } else if (getPosture.get(1) == "ยกนิ้วเท้า") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt41.mp4?alt=media&token=90adaf10-ad65-4589-b4d3-5487c49d7f7e"))
            binding.textView.text = getString(R.string.pt41)

        } else if (getPosture.get(1) == "วิดพื้น") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt51.mp4?alt=media&token=38690341-9ef2-41e3-b983-1b3510cf2bb3"))
            binding.textView.text = getString(R.string.pt51)

        } else if (getPosture.get(1) == "ท่าหมุนบิดดัมเบลด้วยข้อมูล") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt51.mp4?alt=media&token=38690341-9ef2-41e3-b983-1b3510cf2bb3"))
            binding.textView.text = getString(R.string.pt61)

        } else if (getPosture.get(1) == "ท่าย่อตัวด้วยขาข้างเดียว") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt71.mp4?alt=media&token=40bce415-4036-421a-9692-6bc8e4a0b8c8"))
            binding.textView.text = getString(R.string.pt71)

        } else if (getPosture.get(1) == "ท่ายกดัมเบลด้วยไหล่") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt81.mp4?alt=media&token=09ff4478-56a8-4615-b873-29899ef0d452"))
            binding.textView.text = getString(R.string.pt81)

        } else if (getPosture.get(1) == "ท่านอนตะแคงวิดพื้นด้วยแขนหนึ่งข้าง") {
            videoView!!.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/taochizpafit-97499.appspot.com/o/uploads%2Fpt91.mp4?alt=media&token=ed10dfef-8d3d-4fc9-b60c-17316af90fc7"))
            binding.textView.text = getString(R.string.pt91)
        }
        setFavorite(getPosture.get(1))

    }

}
