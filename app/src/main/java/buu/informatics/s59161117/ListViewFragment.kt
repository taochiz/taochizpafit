package buu.informatics.s59161117

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView


class ListViewFragment(var context: Context?, var arrayList: ArrayList<ListView>): BaseAdapter() {
    private class ViewHolder(row: View?){
        var textName: TextView = row?.findViewById(R.id.listviewText) as TextView
        var imageName: ImageView = row?.findViewById(R.id.listviewImage) as ImageView

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View?
        var viewHolder: ViewHolder
        if(convertView == null){
            var layout = LayoutInflater.from(context)
            view = layout.inflate(R.layout.fragment_listview,parent,false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        var listView:ListView = getItem(position) as ListView
        viewHolder.textName.text = listView.name
        viewHolder.imageName.setImageResource(listView.image)
        return view as View

    }

    override fun getItem(position: Int): Any {
        return arrayList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return arrayList.count()
    }

    fun remove(position: Int) {
        arrayList.removeAt(position)
    }
}